package com.learning.abstract_factory;

import java.lang.reflect.Method;

/**
 * 利用反射实现抽象工厂
 */
public class Main {
    public static void main(String[] args) {
        String factory1Name = "com.learning.abstract_factory.Factory1";
        String factory2Name = "com.learning.abstract_factory.Factory2";

        try {
            IFactory factory1 = (IFactory) Class.forName(factory1Name).newInstance();
            IFactory factory2 = (IFactory) Class.forName(factory2Name).newInstance();
            System.out.println("反射生成对象 -- start");
            factory1.createA();
            factory1.createB("hehe.");
            factory2.createA();
            factory2.createB("hehe.");
            System.out.println("反射生成对象 -- end");


            Method[] methods = IFactory.class.getDeclaredMethods();

            System.out.println("反射调用所有方法 -- start");
            for (Method method : methods) {
                if (method.getName().equals("createA")) {
                    method.invoke(factory1);
                    method.invoke(factory2);
                }
                if (method.getName().equals("createB")) {
                    method.invoke(factory1, "hehe");
                    method.invoke(factory2, "hehe");
                }

            }
            System.out.println("反射调用所有方法 -- end");

            System.out.println("反射根据方法名调用方法 -- start");
            Method methodA = IFactory.class.getMethod("createA");
            Method methodB = IFactory.class.getMethod("createB", String.class);
            methodA.invoke(factory1);
            methodA.invoke(factory2);
            methodB.invoke(factory1, "hehe");
            methodB.invoke(factory2, "hehe");

            System.out.println("反射根据方法名调用方法 -- end");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
