package com.learning.abstract_factory;

public class Factory1 implements IFactory {
    @Override
    public void createA() {
        System.out.println("Factory1 createA");
    }

    @Override
    public void createB(String str) {
        System.out.println("Factory1 createB. " + str);
    }
}
