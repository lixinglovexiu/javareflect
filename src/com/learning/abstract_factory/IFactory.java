package com.learning.abstract_factory;

public interface IFactory {
    void createA();
    void createB(String str);
}
