package com.learning.abstract_factory;

public class Factory2 implements IFactory {
    @Override
    public void createA() {
        System.out.println("Factory2 createA");
    }

    @Override
    public void createB(String str) {
        System.out.println("Factory2 createB. " + str);
    }
}
